import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

class RightSide extends React.Component {
    
    constructor (props){
        super(props);
        this.state = { content : ''};
    }

    componentDidMount(){
        axios.get('/load-page',{params:{'url': this.props.url}})
        .then(res => {
            if (res.status == 200) {
                let htmlData = res.data.data;
                this.setState({ content : htmlData });
                
                $(".leftPanel header").remove();
                $(".leftPanel ul.mobileul").remove();
            }
        });
    }

    render(){
        return (
            <div>
                <div dangerouslySetInnerHTML={{__html:this.state.content}}></div>
            </div>
        );
    }
}

export default RightSide;

