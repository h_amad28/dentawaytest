import React from 'react';
import ReactDOM from 'react-dom';
import RightSide from './RightSide';
import {InputTxt} from './HtmlComponents';

function MyComp() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-6 col-md-6 leftPanel">
                    <RightSide />
                </div>

                <div className="col-lg-6 col-md-6">
                    Right Panelzz
                    <InputTxt name="abc" value="" />
                </div>
            </div>
        </div>
    );
}

export default MyComp;

ReactDOM.render(<MyComp />, document.getElementById('root'));