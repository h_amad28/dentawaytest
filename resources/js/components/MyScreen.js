import React from 'react';
import ReactDOM from 'react-dom';
import RightSide from './RightSide';
import {InputTxt, Loading} from './HtmlComponents';

class MyScreen extends React.Component {

	constructor(props){
		super(props);
		this.state = {'data':'', 'jobUrl' : '', "job_url" : ""}
		this.pageHtml = '';
	}

	componentDidMount(){
        axios.get('/load-random')
        .then(res => {
            if (res.status == 200) {
            	let a = res.data.data;
            	Object.entries(a).map((value, key) => {
            		let col_val = {};
            		col_val[ value[0] ] = value[1];
            		this.setState( col_val );
            	})
                // this.setState({ data : res.data.data, jobUrl: res.data.data.job_url });
                this.renderLeftPanel();
            }
        });
	}

	renderLeftPanel() {
		if(this.state.job_url == "")
			return (<Loading />);
		else
			return (<RightSide url={this.state.job_url} />);
	}

	setValue(event){
		this.setState({
			[event.target.name]: event.target.value
		});
	}

	focusSearch(event){
		let val = event.target.value;
		let newHtml = '';
		let HTML = document.getElementById("leftPanel").innerHTML;
		if( this.pageHtml == '' )
			this.pageHtml = HTML;
		console.log(HTML.indexOf(val));
		if (HTML.indexOf(val) > -1){
			//	FIRST METHOD
			newHtml = HTML.replaceAll(val, "<strike style='background:#71d66a;'>" + val + "</strike>");
			//	SECOND METHOD
			// newHtml = HTML.substring(0, HTML.indexOf(val)) + "<strike style='background:#71d66a;'>" + HTML.substring(HTML.indexOf(val), HTML.indexOf(val)+val.length) + "</strike>" + HTML.substring(HTML.indexOf(val) + val.length);;
			document.getElementById("leftPanel").innerHTML = newHtml;
		}

	}

	setBlur(){
		document.getElementById("leftPanel").innerHTML = this.pageHtml;
	}

	render(){
		return (
			<div className="container">
	            <div className="row">
	                <div className="col-lg-6 col-md-6 leftPanel" id="leftPanel">
	                    { this.renderLeftPanel() }
	                
	                </div>

	                <div className="col-lg-6 col-md-6">
	                    <InputTxt name="job_url" onChange={this.setValue.bind(this)} value={this.state.job_url}  />
	                    <InputTxt onBlur={this.setBlur.bind(this)} onFocus={this.focusSearch.bind(this)} name="job_title" label="Job Title" onChange={this.setValue.bind(this)} value={this.state.job_title} />
	                    <InputTxt onBlur={this.setBlur.bind(this)} onFocus={this.focusSearch.bind(this)} name="job_type_id" value="" label="Job Type"  onChange={this.setValue.bind(this)} value={this.state.job_type_id}  />
	                    <InputTxt onBlur={this.setBlur.bind(this)} onFocus={this.focusSearch.bind(this)} name="experience" value="" label="Experience"  onChange={this.setValue.bind(this)} value={this.state.experience}  />
	                    <InputTxt onBlur={this.setBlur.bind(this)} onFocus={this.focusSearch.bind(this)} name="contact_email" value="" label="Contact"  onChange={this.setValue.bind(this)} value={this.state.contact_email}  />
	                </div>
	            </div>
	        </div>
		);
	}

}

export default MyScreen;

ReactDOM.render(<MyScreen />, document.getElementById('root'));