import React from 'react';

const InputTxt = ({name, value, label, onChange, onFocus, onBlur}) => {
	return (<div className="form-group">
		<label htmlFor={name}>{label}</label>
		<input type="text" id={name} name={name} onFocus={onFocus} onBlur={onBlur} onChange={onChange} className="form-control" value={value} />
	</div>);
}

const Loading = () => {
	return (<div className="progress">
	<div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style={{"width": "95%"}}>
	</div>
	</div>);
}

export {InputTxt, Loading}