<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style type="text/css">
            .leftPanel header, .leftPanel ul.mobileul{ display: none; }
        </style>
    </head>
    <body>
        <div class="flex-center full-height">

            <div class="content">
                <div class="title m-b-md">
                    Laravel Quest
                </div>
            </div>
        </div>
        <div id="root"></div>
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript">
            $("document").ready(function(){
                $(".leftPanel header").remove();
                $(".leftPanel ul.mobileul").remove();
            })
        </script>
    </body>
</html>
