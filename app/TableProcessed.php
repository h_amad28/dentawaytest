<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableProcessed extends Model
{
    //
    protected $table = "table_processed";
}
