<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TableProcessed;
use Illuminate\Support\Facades\DB;

class TestProcessedController extends Controller
{
    //

    public function loadSampleData(Request $request)
    {
    	$content = file_get_contents("http://localhost/DentAway/dentaway-challenge/sample.json");
    	$contentList = json_decode($content, true);
    	$requiredList = ['job_url', 'job_description', 'practice_name', 'job_title', 'job_type_id', 'pt_type', 'week_days', 'experience', 'contact_email'];

    	$populate = [];
    	foreach ($contentList as $cTL) {
    		$data = [];
    		foreach ($requiredList as $value) {
    			if (empty($cTL[$value])) {
    				$cTL[$value] = '';
    			}
    			if ($value == 'week_days' && !empty($cTL['week_days'])) {
    				$cTL[$value] = json_encode($cTL['week_days']);
    			}
    			$data[$value] = $cTL[$value];
    		}
    		array_push($populate, $data);
    	}

    	$table = (new TableProcessed())->getTable();
    	DB::table($table)->insert($populate);
    }

    public function loadRandom()
    {
        $row = TableProcessed::where(['is_validated' => 0])->inRandomOrder()->first();

        return response()->json(['data' => $row]);
    }

    public function loadPageData(Request $request)
    {
        if (empty($request->get('url'))) {
            return response()->json(['data' => '']);
        }

        $pageData = file_get_contents($request->get('url'));
        preg_match("/<body.*\/body>/s", $pageData, $matches);
        return response()->json([ 'data' => str_replace(['"/', 'src="/'], ['"https://www.jobtrain.co.uk/', 'src="https://www.jobtrain.co.uk/'], current($matches)) ])->header('Content-Type', 'text/html');
    }
}
