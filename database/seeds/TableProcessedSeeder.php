<?php

use Illuminate\Database\Seeder;
use App\TableProcessed as TableProcessedModel;
use Illuminate\Support\Facades\DB;

class TableProcessedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // print_r(new TableProcessedModel());
    	$table = (new TableProcessedModel())->getTable();
    	DB::table($table)->insert([
    		['job_url' => 'google.com', 'description' => 'tst etest', practice_name => , 'job_title' => '', 'job_type_id' => '', 'pt_type' => , 'week_days' => , 'experience' => , 'contact_email' => ],
    	]);
    }
}
