<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableProcessedCreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_processed', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('job_url', 255);
            $table->string('job_description', 255);
            $table->string('practice_name', 255);
            $table->string('job_title', 50);
            $table->string('job_type_id', 40);
            $table->string('week_days', 255);
            $table->string('experience', 150);
            $table->string('contact_email', 50);
            $table->tinyInteger('pt_type');
            $table->tinyInteger('is_validated');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_processed');
    }
}
